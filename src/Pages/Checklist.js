import React, { Component } from 'react'
import ComponentCheklist from '../Component/ComponentCheklist';

export default class Checklist extends Component {
    constructor(props){
        super(props);
        this.state = {
            data:[]
        }
    }
    componentDidMount(){
        fetch("http://18.141.178.15:8080/v2/checklist", {
            method: "GET",
            credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'eyJhbGciOiJIUzUxMiJ9.eyJyb2xlcyI6W119.i2OVQdxr08dmIqwP7cWOJk5Ye4fySFUqofl-w6FKbm4EwXTStfm0u-sGhDvDVUqNG8Cc7STtUJlawVAP057Jlg'
            })
        })
        .then((response) => {
            console.log(response)
                this.setState({
                    data : response.data.items
                }
                )
        }).catch(error => console.error(error));
    }
    render() {

        return (
            <div>
                               {
                this.state.data.map(data => {
                    return  <ComponentCheklist data={data.name}/>
                })
                }
            </div>
        )
    }
}
